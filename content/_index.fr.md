+++
title = "Home"
description = "Ardour v7.0 tutorial"
+++

# Tutoriel pour Ardour v7.0

**Ardour** est un enregistreur sur disque dur complet et professionnel, ainsi qu'une station de travail audio numérique (STAN). Ardour est un logiciel libre et open source (FLOSS). Il offre un nombre illimité de pistes et de bus audio, une édition non destructive et non linéaire avec annulation illimitée et un routage du signal de n'importe où à n'importe quel endroit. Il prend en charge les formats de fichiers standard, tels que BWF, WAV, WAV64, AIFF et CAF et il peut utiliser les formats de plugins LADSPA, LV2, VST et AudioUnit.

{{< figure src="/ardour-tutorial/images/Ardour6.png" alt="Ardour 6" >}}

Ce tutoriel est une introduction à l'utilisation d'Ardour pour des tâches basiques d'enregistrement et d'édition de sons. 
Il suppose que vous avez déjà installé Ardour sur votre ordinateur.

Pour plus d'informations sur l'installation d'Ardour sur Linux et Mac OS X, veuillez consulter la page [Configuration requise](https://ardour.org/requirements.html). Pour les utilisateurs de Linux, les distributions telles que [KXStudio](http://kxstudio.sourceforge.net/), [UbuntuStudio](http://ubuntustudio.org/) offrent un large choix de logiciels musicaux utiles, dont Ardour.

{{% button href="https://ardour.org/download.html" icon="fas fa-download" %}}Téléchargez la dernière version d'Ardour{{% /button %}}

Contenu :

1. [Introduction](introduction/)
2. [Getting started](getting-started/)
3. [Recording](recording/)
4. [Editing sessions](editing-sessions/)
5. [Mixing sessions](mixing-sessions/)
6. [Exporting sessions](exporting-sessions/)
7. [Saving sessions](saving-sessions/)
8. [Appendices](appendices/)
